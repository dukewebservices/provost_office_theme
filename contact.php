<?php
/*
Template Name: Contact
*/

get_header(); ?>

	<div id="primary" class="site-content">
		<div id="content" role="main">
			<h2><?php the_title(); ?></h2>
		     <div id="page_image">
              <?php the_post_thumbnail();?>
              </div>
              
           <div id="page_content"> 
              
               <div id="map-canvas">
            	<iframe width="350" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=36.001313,+-78.937602&amp;aq=&amp;sll=36.001827,-78.884911&amp;sspn=0.412177,0.675659&amp;t=m&amp;ie=UTF8&amp;ll=36.001313,-78.937602&amp;spn=0.002873,0.004463&amp;z=17&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=36.001313,+-78.937602&amp;aq=&amp;sll=36.001827,-78.884911&amp;sspn=0.412177,0.675659&amp;t=m&amp;ie=UTF8&amp;ll=36.001313,-78.937602&amp;spn=0.002873,0.004463&amp;z=17" style="color:#0000FF;text-align:left">View Larger Map</a></small>
            </div>
           <div id="entry-content">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
		
			<?php endwhile; // end of the loop. 
			?>		
			
			</div>
			
		</div>

		</div><!-- #content -->
	</div><!-- #primary -->
<?php get_sidebar(); ?>

<?php get_footer(); ?>