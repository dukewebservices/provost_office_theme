<?php

/*
Template Name: Resource list
*/


get_header(); ?>


    	<div id="main-wrapper" class="clearfix">
		<div id="primary">
			<div id="content" class="events" role="main">
			   <div id="top_content">
			   
			  <h2><?php the_title(); ?></h2>
			   
			   <div id="page_image">
			   <?php the_post_thumbnail(); ?>
			   </div>
			   
			   
			   
			</div>   
			
		   
			   
			   <div id="page_content">
			      <?php 
			      
			      		$page_data = get_page(10);
			      
			      		//echo"<h2>Reservations</h2>";
			     		echo "<p>".$page_data->post_content."</p>";
			   		?>
			   
			   </div>
			
			   	
			  
			   	<div id="accordion">
			   	<?php  
			   	    $args = array(
							'posts_per_page'  => 1,
							'category_name'   => 'Resource List',
							'orderby'         => 'rand',
							'order'           => 'DESC',
							 );
							 
					  $posts = get_posts($args);
					  
					  
					  
					  foreach($posts as $post):
			   	?>
			   	
			   	
			   		<div class="tlevel clearfix">
			   	    	<div class="icon"></div>
			   	    	<h3><?php echo $post->title; ?></h3>
			   	    	
			   	 	</div>
			   		<div class="blevel">
			   		   

			   		</div>
			   	  
			   	  
			   	  <?php endforeach; ?>
			   		
			   		  		
			   		  	
			   		
			   		</div>
			   
			   
			
			  
			  
			</div><!-- #content -->
		</div><!-- #primary -->

<?php get_sidebar(); ?>		
<?php get_footer(); ?>