<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package Toolbox
 * @since Toolbox 0.1
 */
?>

	</div><!-- #main -->

	<div id="colophon" role="contentinfo">
		<script type="text/javascript" src="https://brandbar.oit.duke.edu/footer/videos_podcasts_full-new.php"></script>
	</div>
	<script type="text/javascript" src="https://brandbar.oit.duke.edu/header/js/globalHeader.js"></script>
	<script type="text/javascript" src="https://brandbar.oit.duke.edu/footer/js/footer.js"></script>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>