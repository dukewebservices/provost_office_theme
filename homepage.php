<?php

/*
Template Name: Homepage
*/


get_header(); ?>


    	<div id="main-wrapper" class="clearfix">
		<div id="primary">
			<div id="content" role="main">
		    <?php
			     	$args = array(
							'posts_per_page'  => 1,
							'category_name'   => 'Homepage Feature',
							'orderby'         => 'rand',
							 );
							 
					  $posts = get_posts($args);
					  
				
					  
					  foreach($posts as $post):
			     
			     ?>
			   
			<div id="featured_image"> 
			   <div id="page_image">
			   <?php the_post_thumbnail(); ?>
			   </div>
			   <div id="page_caption">
			   	<p id="image_caption"><?php echo $post->post_content; ?></p>
			   </div>
			</div>
			   
			  <?php endforeach; ?> 
              
           <div id="page_content">   
			<?php 
			     $page_data = get_page(4);
			     echo "<p>".$page_data->post_content."</p>";
			   ?>
		
			
			
			
			
		
		</div>

		</div><!-- #content -->
		</div><!-- #primary -->

<?php get_sidebar(); ?>		
<?php get_footer(); ?>